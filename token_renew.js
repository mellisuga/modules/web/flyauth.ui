
const XHR = require("core/utils/xhr_async");
const AES = require("./crypt/index.js").AES;

const RSA = require("./rsa/index.js");

const Flyauth = require("./index");

module.exports = async function(unauth_path, table_name, token_name) {

  async function renew_rsa_key() {
    try {
      FLYAUTH_RSA_KEY = await XHR.get("/"+FLYAUTH_PREFIX+"-auth.io", {
        command: "get_public_key"
      });
      console.log("NEW RSA KEY", FLYAUTH_RSA_KEY);
      setTimeout(function() {
        renew_rsa_key()
      }, FLYAUTH_RSA_KEY.exp)
    } catch (e) {
      console.error(e.stack);
    }
  }
  await renew_rsa_key();

  async function renew_token() {
    try {
      let renew_path = table_name ? table_name+"-auth.io-restricted" : "/users-auth.io-restricted";
      console.log(renew_path, token_name);

      let aes = new AES();
      let enc_data = await RSA.encrypt({
        command: "renew-token",
        aes_key: aes.key
      }, XHR);
      let new_token = await XHR.post(renew_path, enc_data, token_name);
      console.log(new_token);
      if (new_token == "UNAUTHORIZED") {
        console.log("UNAUTH");
        /*
        new_token = await new Promise(function(resolve) {
          setTimeout(async function() {
            try {
              aes = new AES();
              let enc_data = await RSA.encrypt({
                command: "renew-token",
                aes_key: aes.key
              }, XHR);
              response = await XHR.post(renew_path, enc_data, token_name)
              response.data = aes.decrypt(response.data);
              resolve(response);
            } catch (e) {
              console.error(e.stack);
            }
          }, 10000);
        });*/
        /*
        let logout_form = document.createElement("form");
        logout_form.method = "POST"
        logout_form.action = unauth_path;
        let cmd_input = document.createElement("input");
        cmd_input.type = "hidden";
        cmd_input.name = "command";
        cmd_input.value = "terminate";
        logout_form.appendChild(cmd_input);
        document.body.appendChild(logout_form);
        logout_form.submit();*/
        window.location.replace(unauth_path);
      } else {
        if (new_token.name && new_token.data) {
          new_token.data = aes.decrypt(new_token.data);
          window.localStorage.setItem(FLYAUTH_PREFIX+"_csrf_token", new_token.data);
          FLYAUTH_UPDATE_AUTHENTIALS(await Flyauth.authentials(FLYAUTH_PREFIX+"_csrf_token", XHR));
          setTimeout(renew_token, new_token.exp*1000);
        } else {
          setTimeout(renew_token, new_token.exp*1000);
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
  if (table_name) await renew_token();

}

