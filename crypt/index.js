const crypto = require('crypto');


module.exports = class {
  static constructor() {

  }
}

module.exports.RSA = class {
  static constructor() {

  }
}

module.exports.AES = class {
  constructor() {
    this.key = crypto.randomBytes(32).toString("base64");
  }

  decrypt(data) {
    const key = Buffer.from(this.key, "base64");

    const iv = Buffer.from(data.substring(0, 24), "base64");
    const auth_tag = Buffer.from(data.substring(24, 48), "base64");
    const encrypted = data.substring(48);

    const decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(auth_tag);
    let str = decipher.update(encrypted, 'base64', 'utf8');
    str += decipher.final('utf8');
    return str;
  }
}
