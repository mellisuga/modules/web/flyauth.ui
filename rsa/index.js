
const crypto = require("crypto");

module.exports = class {
  static async encrypt(data) { // STRING ONLY FOR NOW
    try {
      if (typeof data == "object") data = JSON.stringify(data);
      let chunks = data.match(/.{1,214}/g); // >> (2048/8) - 42 = 214

      let key = FLYAUTH_RSA_KEY;

      let enc_obj = {
        key_id: key.id,
        data: new Uint8Array(256*chunks.length)
      }

      for (let c = 0; c < chunks.length; c++) {
        let data_index = c*256;
        let chunk = crypto.publicEncrypt({ key: key.publicKey }, Buffer.from(chunks[c]));
        enc_obj.data.set(chunk, data_index);
      }


//      enc_obj.data = Array.from(enc_obj.data);
//      enc_obj.data = Buffer.from(enc_obj.data).toString("hex");

/*
      let enc_obj = {
        key_id: key.id,
        data: []
      }

      for (let c = 0; c < chunks.length; c++) {
        enc_obj.data.push(crypto.publicEncrypt({ key: key.publicKey }, Buffer.from(chunks[c])));
      }
*/
      return enc_obj.key_id+Buffer.from(enc_obj.data).toString("hex");
    } catch (e) {
      console.error(e.stack);
    }
  }
}
