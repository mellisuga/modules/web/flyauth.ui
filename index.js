
const Fingerprint = require("./fingerprint/index.js");

const pako = require('pako');

const nunjucks = require("nunjucks");
let login_body = require("./body/login.html");
let register_body = require("./body/register.html");


const RSA = require("./rsa/index.js");
const AES = require("./crypt/index.js").AES;

const XHR = require("core/utils/xhr_async");
const FlyHTML = require("core/flyhtml/index.js");

let lang = {
  lt: require("./lang/lt.json"),
  en: require("./lang/en.json")
}

function validate_email(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

let auth_verification_code = getParameterByName("verification_code");
let auth_success_redirect = getParameterByName("next_url");

let rsa_injected = true;
module.exports = class {

  static async prepare(unauth_path) {
    try {
      if (typeof FLYAUTH_RSA_KEY !== "undefined" && FLYAUTH_RSA_KEY) {
        async function renew_rsa_key() {
          try {
            console.log("renew_rsa_key");
            if (!rsa_injected) {
              FLYAUTH_RSA_KEY = await XHR.get("/"+FLYAUTH_PREFIX+"-auth.io", {
                command: "get_public_key"
              });
              FLYAUTH_RSA_KEY.delayed_exp_timestamp = Date.now()+FLYAUTH_RSA_KEY.exp+FLYAUTH_RSA_KEY.exp_delay;
            } else {
              rsa_injected = false;
            }
            if (typeof FLYAUTH_UPDATE_AUTHENTIALS !== 'undefined') FLYAUTH_UPDATE_AUTHENTIALS(await module.exports.authentials(FLYAUTH_PREFIX+"_csrf_token"));
            setTimeout(function() {
              renew_rsa_key()
            }, FLYAUTH_RSA_KEY.exp)
          } catch (e) {
            console.error(e.stack);
          }
        }
        await renew_rsa_key();
      }


      if (typeof FLYAUTH_SESSION_DATA !== "undefined" && FLYAUTH_SESSION_DATA) {
        async function renew_token() {
          try {
            let renew_path = "/"+FLYAUTH_PREFIX+"-auth.io-restricted";

            let aes = new AES();
            let enc_data = await RSA.encrypt({
              command: "renew-token",
              aes_key: aes.key
            }, XHR);
            let new_token = await XHR.post(renew_path, enc_data, true);
            console.log("NEW TOKEN", new_token);
            if (new_token == "UNAUTHORIZED") {
              console.log("UNAUTH");
              if (unauth_path) window.location.replace(unauth_path);
            } else {
              if (new_token.name && new_token.data) {
                new_token.data = aes.decrypt(new_token.data);

                AUTHENTIALS_DEXP = Date.now()+(new_token.exp+new_token.exp_delay)*1000;

                window.localStorage.setItem(FLYAUTH_PREFIX+"_csrf_token", new_token.data+AUTHENTIALS_DEXP);
                FLYAUTH_UPDATE_AUTHENTIALS(await module.exports.authentials(FLYAUTH_PREFIX+"_csrf_token"));
                setTimeout(renew_token, new_token.exp*1000);
              } else {
                setTimeout(renew_token, new_token.exp*1000);
              }
            }
          } catch (e) {
            console.error(e.stack);
          }
        }
        let renew_timeout = AUTHENTIALS_DEXP-Date.now()-FLYAUTH_TOKEN_EXPD*1000;
        console.log(renew_timeout);
        setTimeout(() => { renew_token() }, renew_timeout);
      }

    } catch (e) {
      console.error(e.stack);
    }
  }

  /**
   * @static
   * @method login
   * @arg {Object} cfg Configuration 
   * @arg {String} cfg.lang Language i.e. `en`
   * @arg {String} cfg.auth_path authentication path i.e. `/admin-auth.io`
   * @returns {DOMElement}
   */
  static login(cfg) {
    if (!cfg) cfg = {};

    if (cfg.success_path) auth_success_redirect = cfg.success_path;

    let lkey = cfg.lang || "lt";
    let body = nunjucks.renderString(login_body, { lang: lang[lkey] });

    let element = document.createElement("div");
    element.classList.add("flyauth_block");
    element.innerHTML = body;

    let topt_failures = 0;
    let emailed_code = false;

    let loading_block = element.querySelector(".auth_loading");

    let err_msg = element.querySelector(".err_msg");

    let email_step = element.querySelector(".auth_step_email");

    let email_step_input = element.email_input = email_step.querySelector("input");
    email_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        email_step_button.click();
      }
    });

    let email_step_button = email_step.querySelector("button");
    email_step_button.addEventListener("click", function() {
      err_msg.innerHTML = "";
      if (validate_email(email_step_input.value)) {
        email_step.style.display = "none";
        pwd_step.style.display = "";
        pwd_step_input.focus();
      } else {
        err_msg.innerHTML = lang[lkey].signin.error.invalid_email;
      }
    });

    async function authenticate(usr, pwd, hash, otp_token) {
      try {
        if (pwd.length > 0) {
          loading_block.style.display = "";

          let aes = new AES();
          let auth_data = {
            command: "authenticate",
            email: usr,
            pwd: pwd,
            totp_token: otp_token,
            fp: JSON.stringify(await Fingerprint.get()),
            aes_key: aes.key
          };
          if (hash) auth_data.security_hash = hash;

          let d1 = Date.now();
          console.log("TOKEN CHECK");

          console.log(XHR);
          let encrypted_auth_data = await RSA.encrypt(JSON.stringify(auth_data), XHR);
          let resp = await XHR.post("/"+FLYAUTH_PREFIX+"-auth.io", encrypted_auth_data);
          console.log("TOOK:", Date.now() - d1);
          console.log(resp);
          if (typeof resp === 'object') {
            let texp_str = Date.now()+(FLYAUTH_TOKEN_EXP+FLYAUTH_TOKEN_EXPD)*1000;
            window.localStorage.setItem(FLYAUTH_PREFIX+"_csrf_token", aes.decrypt(resp.access_token)+texp_str);
            window.location.replace(auth_success_redirect || resp.next_path || "/mellisuga");
            await new Promise(function(resolve) {
              setTimeout(function() {
                resolve();
              }, 1000 * 60 * 60);
            });
          }
          loading_block.style.display = "none";
          return resp;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e.stack);
      }
    }

    let pwd_step = element.querySelector(".auth_step_pwd");

    let pwd_step_input = pwd_step.querySelector("input");
    pwd_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        pwd_step_button.click();
      }
    });

    let pwd_back_button = pwd_step.querySelector("button:first-child");
    pwd_back_button.addEventListener("click", function(e) {
      email_step.style.display = "";
      email_step_input.focus();
      pwd_step.style.display = "none";
    });

    let pwd_step_button = pwd_step.querySelector("button:last-child");
    pwd_step_button.addEventListener("click", async function(ev) {
      try {
        err_msg.innerHTML = "";
        pwd_step.style.display = "none";
        let resp = await authenticate(email_step_input.value, pwd_step_input.value);
        if (typeof resp !== 'object' ) {
          err_msg.innerHTML = lang[lkey].signin.error.usr_pwd;
        }
        if (resp == "SECURITY_CODE_REQUIRED") {
          emailed_code = true;
          err_msg.innerHTML = lang[lkey].signin.error.usr_pwd_code;
          code_step.style.display = "";
          code_step_input.focus();
        } else if (resp == "TOTP_TOKEN_REQUIRED") {
          if (topt_failures > 0) {
            err_msg.innerHTML = lang[lkey].signin.error.usr_pwd_code;
          } else {
            err_msg.innerHTML = "";
          }
          otp_step.style.display = "";
          otp_step_input.focus();
          topt_failures++;
        } else {
          pwd_step.style.display = "";
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    let code_step = element.querySelector(".auth_step_code");

    let code_step_input = code_step.querySelector("input");
    code_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        code_step_button.click();
      }
    });

    let code_back_button = code_step.querySelector("button:first-child");
    code_back_button.addEventListener("click", function(e) {
      pwd_step.style.display = "";
      pwd_step_input.focus();
      code_step.style.display = "none";
    });

    let code_step_button = code_step.querySelector("button:last-child");
    code_step_button.addEventListener("click", async function(ev) {
      try {
        err_msg.innerHTML = "";
        code_step.style.display = "none";
        let resp = await authenticate(email_step_input.value, pwd_step_input.value, code_step_input.value);
        if (resp == "SECURITY_CODE_REQUIRED") {
          err_msg.innerHTML = lang[lkey].signin.error.usr_pwd_code;
          code_step.style.display = "";
        } else if (resp == "TOTP_TOKEN_REQUIRED") {
          if (topt_failures > 0) {
            err_msg.innerHTML = lang[lkey].signin.error.usr_pwd_code;
          } else {
            err_msg.innerHTML = "";
          }
          otp_step.style.display = "";
          otp_step_input.focus();
          topt_failures++;
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    let otp_step = element.querySelector(".auth_step_totp");

    let otp_step_input = otp_step.querySelector("input");
    otp_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        otp_step_button.click();
      }
    });

    let otp_back_button = otp_step.querySelector("button:first-child");
    otp_back_button.addEventListener("click", function(e) {
      otp_step.style.display = "none";
      if (emailed_code) {
        code_step.style.display = "";
        code_step_input.focus();
      } else {
        pwd_step.style.display = "";
        pwd_step_input.focus();
      }
    });

    let otp_step_button = otp_step.querySelector("button:last-child");
    otp_step_button.addEventListener("click", async function(ev) {
      try {
        err_msg.innerHTML = "";
        otp_step.style.display = "none";
        let resp = await authenticate(email_step_input.value, pwd_step_input.value, code_step_input.value, otp_step_input.value);
        if (resp == "SECURITY_CODE_REQUIRED") {
          err_msg.innerHTML = lang[lkey].signin.error.usr_pwd_code;
          otp_step.style.display = "";
        } else if (resp == "TOTP_TOKEN_REQUIRED") {
          if (topt_failures > 0) {
            err_msg.innerHTML = lang[lkey].signin.error.usr_pwd_code;
          } else {
            err_msg.innerHTML = "";
          }
          otp_step.style.display = "";
          otp_step_input.focus();
          topt_failures++;
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    return element;
  }

  /**
   * @static
   * @method login
   * @arg {Object} paths All paths needed for the user interface 
   * @arg {String} paths.privacy Relative url path to pricavy policy
   * @arg {String} paths.agreement Relative url path to user agreement
   * @arg {String} paths.success Relative url path to signin page
   * @arg {Object} cfg Configuration 
   * @arg {String} cfg.lang Language i.e. `en`
   * @arg {String} cfg.auth_path authentication path i.e. `/admin-auth.io`
   * @returns {DOMElement}
   */
  static register(paths, cfg) {
    if (!cfg) cfg = {};
    let lkey = cfg.lang || "lt";

    let step_form = undefined;

    let form_steps = [
      {
        type: "text",
        name: "email",
        placeholder: lang[lkey].signup.placeholder.email,
        verify: async function(value, values, errcb) {
          try {
            if (validate_email(value)) {
              step_form.display_loading();
              let encrypted_auth_data = await RSA.encrypt(JSON.stringify({
                command: "signup_email",
                email: value
              }), XHR);
              let resp = await XHR.post("/"+FLYAUTH_PREFIX+"-auth.io", encrypted_auth_data);
              // TODO : CHECK FOR EMAIL EXISTS ERROR
              return true;
            }
            errcb(lang[lkey].signup.error.invalid_email)
            return false;
          } catch (e) {
            console.error(e.stack);
            return false;
          }
        }
      },
      {
        type: "text",
        name: "code",
        placeholder: lang[lkey].signup.placeholder.security_code,
        verify: async function(value, values, errcb) {
          try {
            if (value.length == 32) return true;
            errcb(lang[lkey].signup.error.invalid_code)
            return false;
          } catch (e) {
            console.error(e.stack);
            return false;
          }
        }
      },
      {
        type: "password",
        name: "pwd",
        placeholder: lang[lkey].signup.placeholder.pwd1,
        verify: async function(value, values, errcb) {
          try {
            if (value.length > 4) {
              return true;
            }
            errcb(lang[lkey].signup.error.pwd_too_short)
            return false;
          } catch (e) {
            console.error(e.stack);
            return false;
          }
        }
      },
      {
        type: "password",
        placeholder: lang[lkey].signup.placeholder.pwd2,
        verify: async function(value, values, errcb) {
          try {
            if (values["pwd"] === value) {
              step_form.display_loading();
              let auth_verification_id = auth_verification_code;
              if (auth_verification_id !== null) {
                auth_verification_code = auth_verification_id.slice(0, 32);
                auth_verification_id = atob(decodeURIComponent(auth_verification_id.slice(32)));
              }

              let data_obj = {
                command: "register",
                email: auth_verification_id ? undefined : values["email"],
                verification_id: auth_verification_id,
                code: auth_verification_code || values["code"],
                pwd: value
              }

              for (let i = 0; i < cfg.extra_inputs.length; i++) {
                let extra_input = cfg.extra_inputs[i];
                data_obj[extra_input.name] = values[extra_input.name];
              }

              let encrypted_auth_data = await RSA.encrypt(JSON.stringify(data_obj), XHR);
              let resp = await XHR.post("/"+FLYAUTH_PREFIX+"-auth.io", encrypted_auth_data);

              console.log(resp);
              if (resp == "SUCCESS") {
                return true;
              } else {
                errcb(lang[lkey].signup.error.invalid_code)
                return false;
              }          

            }

            errcb(lang[lkey].signup.error.pwd_match)
            return false;
          } catch (e) {
            console.error(e.stack);
            return false;
          }
        }
      }
    ];

    for (let i = 0; i < cfg.extra_inputs.length; i++) {
      let extra_input = cfg.extra_inputs[i];
      form_steps.splice(extra_input.index, 0, extra_input);
    }

    step_form = new FlyHTML.StepForm({
      instructions: lang[lkey].signup.instructions
    },form_steps, async function(values) {
      console.log("FORM VALUES", values);
      window.location.replace(paths.success);
    });

    if (auth_verification_code !== null) {
      step_form.steps.splice(0, 2);
    }

    step_form.element.classList.add("flyauth_block");
/*
    let body = nunjucks.renderString(register_body, {
      paths: paths,
      lang: lang[lkey]
    });

    let element = document.createElement("div");
    element.classList.add("flyauth_block");
    element.innerHTML = body;

    let instructions = element.querySelector("h3");


    let loading_block = element.querySelector(".auth_loading");

    let err_msg = element.querySelector(".err_msg");

    let agree_block = element.querySelector(".auth_agreements");
    let agree = element.querySelector(".auth_agreements input");

    let email_step = element.querySelector(".auth_step_email");

    let email_step_input = element.email_input = email_step.querySelector("input");
    email_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        email_step_button.click();
      }
    });

    let email_step_button = email_step.querySelector("button");
    email_step_button.addEventListener("click", async function() {
      try {
        err_msg.innerHTML = "";
        if (validate_email(email_step_input.value)) {
          if (!agree.checked) {
            err_msg.innerHTML = lang[lkey].signup.error.agree;
          } else {
            agree_block.style.display = "none";
            email_step.style.display = "none";
            loading_block.style.display = "";

            let encrypted_auth_data = await RSA.encrypt(JSON.stringify({
              command: "signup_email",
              email: email_step_input.value
            }), XHR);
            let resp = await XHR.post("/"+FLYAUTH_PREFIX+"-auth.io", encrypted_auth_data);
     
            loading_block.style.display = "none";
            code_step.style.display = "";
            code_step_input.focus();
          }
        } else {
          err_msg.innerHTML = lang[lkey].signup.error.invalid_email;
        }
      } catch (e) {
        console.error(e.stack);
      }
    });
    
    let code_step = element.querySelector(".auth_step_code");

    let code_step_input = code_step.querySelector("input");
    code_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        code_step_button.click();
      }
    });

    let code_back_button = code_step.querySelector("button:first-child");
    code_back_button.addEventListener("click", function(e) {
      email_step.style.display = "";
      email_step_input.focus();
      code_step.style.display = "none";
    });

    let code_step_button = code_step.querySelector("button:last-child");
    code_step_button.addEventListener("click", async function(ev) {
      try {
        err_msg.innerHTML = "";
        if (code_step_input.value.length == 32) {
          code_step.style.display = "none";
          pwd1_step.style.display = "";
          pwd1_step_input.focus();
        } else {
          err_msg.innerHTML = lang[lkey].signup.error.invalid_code;
        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    let pwd1_step = element.querySelector(".auth_step_pwd1");

    let pwd1_step_input = pwd1_step.querySelector("input");
    pwd1_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        pwd1_step_button.click();
      }
    });

    let pwd1_back_button = pwd1_step.querySelector("button:first-child");
    pwd1_back_button.addEventListener("click", function(e) {
      code_step.style.display = "";
      code_step_input.focus();
      pwd1_step.style.display = "none";
    });

    let pwd1_step_button = pwd1_step.querySelector("button:last-child");
    pwd1_step_button.addEventListener("click", async function(ev) {
      try {
        pwd1_step.style.display = "none";
        pwd2_step.style.display = "";
        pwd2_step_input.focus();

      } catch (e) {
        console.error(e.stack);
      }
    });

    let auth_verification_id = undefined;
    if (auth_verification_code) {
      email_step.style.display = "none";
      agree_block.style.display = "none";
      pwd1_step.querySelector("button").style.display = "none";
      pwd1_step.style.marginLeft = "50px";
      pwd1_step.style.display = "";
      pwd1_step_input.focus();
      auth_verification_id = auth_verification_code;
      auth_verification_code = auth_verification_id.slice(0, 32);
      auth_verification_id = atob(decodeURIComponent(auth_verification_id.slice(32)));
    } else if (cfg.invitation_only) {
      email_step.style.display = "none";
      agree_block.style.display = "none";
      instructions.style.display = "none";
      err_msg.innerHTML = lang[lkey].signup.error.invitation_only;
      err_msg.style.marginTop = "53px";
    }

    let pwd2_step = element.querySelector(".auth_step_pwd2");

    let pwd2_step_input = pwd2_step.querySelector("input");
    pwd2_step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        pwd2_step_button.click();
      }
    });

    let pwd2_back_button = pwd2_step.querySelector("button:first-child");
    pwd2_back_button.addEventListener("click", function(e) {
      pwd1_step.style.display = "";
      pwd1_step_input.focus();
      pwd2_step.style.display = "none";
    });

    let pwd2_step_button = pwd2_step.querySelector("button:last-child");
    pwd2_step_button.addEventListener("click", async function(ev) {
      try {
        err_msg.innerHTML = "";
        if (pwd1_step_input.value.length > 4 && pwd1_step_input.value == pwd2_step_input.value) {
          pwd2_step.style.display = "none";

          loading_block.style.display = "";
          let encrypted_auth_data = await RSA.encrypt(JSON.stringify({
            command: "register",
            email: auth_verification_id ? undefined : email_step_input.value,
            verification_id: auth_verification_id,
            code: auth_verification_code || code_step_input.value,
            pwd: pwd2_step_input.value
          }), XHR);
          let resp = await XHR.post("/"+FLYAUTH_PREFIX+"-auth.io", encrypted_auth_data);

          console.log(resp);
          if (resp == "SUCCESS") {
            window.location.replace(paths.success);
          } else {
            loading_block.style.display = "none";
            code_step.style.display = "";
            err_msg.innerHTML = lang[lkey].signup.error.invalid_code;
          }          
        } else {
          if (pwd1_step_input.value.length <= 4) {
            err_msg.innerHTML = lang[lkey].signup.error.pwd_too_short;
          } else {
            err_msg.innerHTML = lang[lkey].signup.error.pwd_match;
          }
        }
      } catch (e) {
        console.error(e.stack);
      }
    });
*/

    return step_form;
  }

  static async authentials(token_name) {
    try {
      let csrf = localStorage.getItem(token_name) ? localStorage.getItem(token_name).substring(0, 128) : undefined;

      if (csrf) {
        let credentials = {
          fp: await Fingerprint.get(),
          csrf: csrf
        };

        let encreds = await RSA.encrypt(JSON.stringify(credentials));

        return encodeURIComponent(Buffer.from(pako.deflate(Uint8Array.from(Buffer.from(encreds, 'hex')))).toString("base64"));
      } else {
        return undefined;
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}

module.exports.RSA = RSA;
module.exports.Fingerprint = Fingerprint;
