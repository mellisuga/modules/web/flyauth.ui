
const FingerprintJS = require("@fingerprintjs/fingerprintjs");

const required_components = [
  "cpuClass",
  "deviceMemory",
  "hardwareConcurrency",
  "osCpu",
  "platform",
  "timezone",
  "touchSupport"
];

function compress_fp(fp) {
  return [
    fp.cpuClass,
    fp.deviceMemory,
    fp.hardwareConcurrency,
    fp.osCpu,
    fp.platform,
    fp.timezone,
    fp.touchSupport.maxTouchPoints,
    fp.touchSupport.touchEvent ? 1 : 0,
    fp.touchSupport.touchStart ? 1 : 0
  ];
}

module.exports = class {
  static async get() {
    try {
      let fp = await FingerprintJS.load();

      let res_fp = (await fp.get()).components;
      let fp_obj = {};
      for (let f in res_fp) {
        if (required_components.includes(f)) fp_obj[f] = res_fp[f].value;
      }

      return compress_fp(fp_obj);
    } catch (e) {
      console.error(e.stack);
    }
  }
}
